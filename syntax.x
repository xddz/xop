/* as you
 * can see
 * /* xop */ supports /* nested comments */ !
*/
// - - - - - - - - - - - - - - - - - 
// file: "prog.x"
// - - - - - - - - - - - - - - - - - 
#include std; // as standard

u0 print(string : str) {
  std::print(string);
}

// - - - - - - - - - - - - - - - - - 
// file: "main.x"
// - - - - - - - - - - - - - - - - - 
#include "prog.x"; // as program

s32 main(u0) {
  some_str : str = "Hello, World!\n";
  some_str[0] = 'Y';
  prog::print(some_str);
}

// - - - - - - - - - - - - - - - - - 
