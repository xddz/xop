# xop

![xop](xop.png)

* [❗] Warning: **xop is still a work in progress.**

* [What's xop?](#whats-xop)
* [Features](#features)
  * [Portability](#portability)
  * [Performance](#performance)
  * [Safety](#safety)
* [Architecture](#architecture)
* [Getting started](#getting-started)
  * [Building the project](#building-the-project)
  * [Compiling a file](#compiling-a-file)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)

<p align="right">(<a href="#top">back to top</a>)</p>

## What's xop?
**xop** is a high-performant portable statically-typed programming language.
![xop](out.png)

<p align="right">(<a href="#top">back to top</a>)</p>

## Features
  * [Portability](#portability)
  * [Performance](#performance)
  * [Safety](#safety)

<p align="right">(<a href="#top">back to top</a>)</p>

## Portability
**xop** compiles to fast cross-platform C99 code, making it possible for any CPU Architecture or Operating System with a C99 compiler available to compile **xop** code.

<p align="right">(<a href="#top">back to top</a>)</p>

## Performance
**xop** compiles to nicely optimized cross-platform C99 code with **minimal to 0 overhead**.

<p align="right">(<a href="#top">back to top</a>)</p>

## Safety
**xop** uses safe C functions by default, allowing for unsafe code if needed.

<p align="right">(<a href="#top">back to top</a>)</p>

## Architecture

* [❗] Warning: **xop is still a work in progress.**

**xop** is made specifically with performance and stability as it's top priorities.
With that in mind, **everything (besides the C standard libraries)** is made completely from scratch, by the author himself.
This *from-scratch* approach allows for much much scalable code, designed specifically for **xop**.

![graph](graph.png)

<p align="right">(<a href="#top">back to top</a>)</p>

## Getting Started

### Building the project
```bash
$ git clone "https://gitlab.com/xddz/xop.git"
$ cd xop
$ ./build.sh # ./build.sh - windows
```

### Compiling a file

* [❗] Warning: **xop is still a work in progress.**

**hello.x:**
```
fn main() {
  print("Hello, World!\n");
}
```
**Compiling:**
```
$ xop -c hello.x -o hello
$ ./hello
Hello, World!
```

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/MyFeature`)
3. Commit your Changes (`git commit -m 'Added MyFeature'`)
4. Push to the Branch (`git push origin feature/MyFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>

## License

Distributed under the Open Data Commons Open Database License (ODbL). See `LICENSE` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>

## Contact

 * Twitter: [@_xddz_](https://twitter.com/_xddz_)
 * Mastodon: [@xddz@noc.social](https://noc.social/@xddz)
 * Email: [xddz@tuta.io](mailto:xddz@tuta.io)

Project Link: [https://gitlab.com/xddz/xop](https://gitlab.com/xddz/xop)

<p align="right">(<a href="#top">back to top</a>)</p>
