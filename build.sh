#!/bin/bash
set -e

EXE="xop"
DEFINES=""
DDEFINES="-D_DEBUG"

DIR="./src/"
TMP="./.bsh/"
OUT="./bin/"

FLAGS="-I.$DIR -Wextra -D_FORTIFY_SOURCE=2 -D_GLIBCXX_ASSERTIONS -fstack-clash-protection -Wshadow -Wformat=2 -Wformat-truncation -Wformat-overflow -fno-common -fstack-usage"

case "$1" in
"--debug"|"-d")
printf "[+] Debug mode\n"
LFLAGS="-lm -g"
CFLAGS="-O $LFLAGS $DDEFINES $FLAGS"
;;
*)
printf "[+] Release mode\n"
LFLAGS="-lm -static -static-libgcc"
CFLAGS="-O3 $LFLAGS $DEFINES $FLAGS"
;;
esac

case "$2" in
"windows"|"win")
printf "[#] Windows\n"
CC=i686-w64-mingw32-gcc
;;
*)
printf "[+] GNU/Linux\n"
CC=gcc
;;
esac

[ ! -d "$TMP" ] && mkdir "$TMP"
cd "$TMP"
printf "[+] Compiling:\n"

for cfile in $(find ".$DIR" | grep "\.c$"); do
compiled=$(basename "$cfile").o
compiled=${compiled#".$DIR"}

if [ -s $compiled ]; then
  #file exists and is not empty
  csource=${compiled%.*}.c
  STATUS="$(cmp --silent $csource $cfile; echo $?)"
  if [ $STATUS -ne 0 ]; then
    # files differ
    cp $cfile -r .
    printf "$CC -c $cfile $CFLAGS -o $compiled\n"
    $CC -c $cfile $CFLAGS -o $compiled
  fi
else
  cp $cfile -r .
  printf "$CC -c $cfile $CFLAGS -o $compiled\n"
  $CC -c $cfile $CFLAGS -o $compiled
fi
done

cd ..

[ ! -d "$OUT" ] && mkdir "$OUT"
printf "$CC $TMP*.o -o $OUT$EXE\n"
$CC $(find $TMP*.o) $LFLAGS -o "$OUT$EXE"
