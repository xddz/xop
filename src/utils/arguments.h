#pragma once

#include <defines.h>

#define _STD_ARGS_IMPL
#include <std/_std_args.h>

b8 parse_arguments(s32 * argc, char *** argv);

u0 print_banner(u0);
u0 print_usage(const char * const basename,
               const argument_context * const restrict arg_ctx,
               const u8 padding);
