#include "arguments.h"

#define ARG_USAGE_PADDING (30)

static const char * const restrict banner =\

" __  __    ___    \n"
" \\ \\/ /   /   \\   \n"
"  >  <    > | <   \n"
" /_/\\_\\   \\___/   \n"
"      _________           xop - " XOP_VERSION " \n"
"      \\__   _  |  \n"
"         | |_| |  \n"
"          -----   \n";

b8 parse_arguments(s32 * argc, char *** argv) {
  argument_data a_data[4] = {
    {
      .identifier   = 'h',
      .access_chars = "h",
      .access_name  = "help",
      .description  = "Display the help menu."
    },

    {
      .identifier = 'i',
      .access_chars = "i",
      .access_name = "init",
      .description  = "Initialize a project."
    },

    {
      .identifier = 'c',
      .access_chars = "c",
      .access_name = "compile",
      .value_name = "<FILE.x>",
      .description  = "Compile a file."
    },

    {
      .identifier = 'o',
      .access_chars = "o",
      .access_name = "output",
      .value_name = "<OUTPUT>",
      .description  = "Output executable to <OUTPUT>"
    },
  };

  argument_context a_ctx = arg_init(argc, argv, a_data, 4);
  if (*argc < 2) {
    print_usage(**argv, &a_ctx, ARG_USAGE_PADDING);
    return false;
  }

  char identifier = 0;
  while (_STD_ARG_FINISHED != (identifier = arg_fetch(&a_ctx))) {
    switch (identifier) {
      default: {
        print_usage(**argv, &a_ctx, ARG_USAGE_PADDING);
        _STD_FATAL("Invalid argument: `%s`, id = [%d]", *a_ctx.argv, identifier);
        return false;
      }

      case _STD_ARG_UNDEFINED: {
        print_usage(**argv, &a_ctx, ARG_USAGE_PADDING);
        _STD_FATAL("Invalid argument: `%s`", *a_ctx.argv);
        return false;
      }

      case _STD_ARG_MISSING: {
        print_usage(**argv, &a_ctx, ARG_USAGE_PADDING);
        _STD_FATAL("Missing parameter to argument `%s`", *a_ctx.argv);
        return false;
      }

      case 'h': {
        print_usage(**argv, &a_ctx, ARG_USAGE_PADDING);
        break;
      }

      case 'i': {
        printf("Initializing a new project.\n");
        break;
      }

      case 'c': {
        while (a_ctx.argc > 1 && *arg_peek_forward(&a_ctx) != '-') {
          printf("Building file: `%s`\n", arg_arg(&a_ctx));
        }
        break;
      }

      case 'o': {
        printf("Outputting to file: `%s`\n", arg_arg(&a_ctx));
        break;
      }
    }
  }
  return true;
}

u0 print_banner(u0) {
  _STD_COLOR(_STD_COLOR_PURPLE, true);
  printf("%s", banner);
  _STD_BLANK();
}

u0 print_usage(const char * const basename,
               const argument_context * const restrict arg_ctx,
               const u8 padding) {

  print_banner();

  _STD_COLOR(_STD_COLOR_WHITE, true);
  printf("USAGE:");
  _STD_BLANK();

  printf("\n  %s <OPTIONS> <FILE.x>\n", basename);

  _STD_COLOR(_STD_COLOR_WHITE, true);
  printf("OPTIONS:");
  _STD_BLANK();

  putchar('\n');
  arg_usage(arg_ctx, padding);
}
