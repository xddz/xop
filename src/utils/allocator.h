#pragma once

#include <std/_std_types.h> /* fixed-width datatypes    */
#include <std/_std_log.h>   /* custom logging functions */

#define _STD_ARENA_IMPL
#include <std/_std_arena.h> /* custom arena allocator   */

u0 _mem_init(u0);
u0 _mem_free(u0);
u0 * _mem_allocate(u0 * data, const u8 bytes);

#define _STD_STR_IMPL
#define _STD_ALLOCATE(buffer, length) buffer = _mem_allocate(buffer, length)
#define _STD_REALLOCATE
#define _STD_FREE
//#define _STD_STRNCPY xxx
//#define _STD_STRNCAT xxx
#include <std/_std_str.h>   /* custom string library    */
