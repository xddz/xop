#include "allocator.h"

/* internal buffer of size 2^16 */
static u8 internal_storage[65536];

/* internal arena */
static arena_t arena = {0};

u0 _mem_init(u0) {
  arena_init(&arena, internal_storage, 65536);
}

u0 _mem_free(u0) {
  arena_free(&arena);
}

u0 * _mem_allocate(u0 * data, const u8 bytes) {
  return arena_alloc(&arena, bytes);
}
