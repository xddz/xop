#pragma once

#define XOP_VERSION "0.02"

/* #define _STD_DEBUG */
#include <std/_std_types.h>  /* fixed-width datatypes    */
#include <std/_std_log.h>    /* custom logging functions */

#include <utils/allocator.h> /* custom allocator         */
