#include <defines.h>
#include <utils/arguments.h>

s32 main(s32 argc, char **argv) {
  if (!parse_arguments(&argc, &argv)) {
    return -1;
  }

  _mem_init();
  /* compiler code */
  _mem_free();
  return 0;
}

