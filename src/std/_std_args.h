/* * * * * * * * * * * * * * * * *
 *       _       _       _       *
 *  ___ | |_  __| |     | |__    *
 * / __|| __|/ _` |     | '_ \   *
 * \__ \| |_| (_| |  _  | | | |  *
 * |___/ \__|\__,_| (_) |_| |_|  *
 *                               *
 * * * * * * * * * * * * * * * * *
 *       *  _std_args.h  *       *
 * * * * * * * * * * * * * * * * *
 *    https://xddz.pages.dev     *
 * * * * * * * * * * * * * * * * *
 * #define _STD_ARGS_IMPL        *
 * #include "_std_args.h"        *
 *                               *
 * argument_data a_data[2] = {   *
 *   {                           *
 *     .identifier   = 'a',      *
 *     .access_chars = "1",      *
 *     .access_name  = "test1",  *
 *     .description  = "..."     *
 *   },                          *
 *                               *
 *   {                           *
 *     .identifier   = 'b',      *
 *     .access_chars = "2",      *
 *     .access_name  = "test2",  *
 *     .description  = "..."     *
 *   },                          *
 * };                            *
 *                               *
 * argument_context a_ctx =\     *
 *  arg_init(&argc,              *
 *           &argv,              *
 *           a_data,             *
 *           2);                 *
 *                               *
 * if (argc < 2) {               *
 *   print_usage(*argv,          *
 *               &a_ctx,         *
 *               30);            *
 *   return 1;                   *
 * }                             *
 *                               *
 * char identifier = 0;          *
 * while ((identifier =\         *
 *   arg_fetch(&a_ctx)) > 1) {   *
 * }                             *
 * * * * * * * * * * * * * * * * */

#ifndef _STD_ARGS_H
#define _STD_ARGS_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _STD_TYPES_H
#include <inttypes.h>

#ifndef uptr
#define uptr uintptr_t
#endif

#ifndef u64
#define u64 uint64_t
#endif

#ifndef u8
#define u8 uint8_t
#endif

#ifndef u0
#define u0 void
#endif
#endif /* _STD_TYPES_H */

#ifndef _STD_LOG_H
#include <stdio.h>
#endif /* _STD_LOG_H */

#ifndef _STD_FN
#define _STD_FN inline static
#endif /* _STD_FN */

#ifndef _STD_ARG_UNDEFINED
#define _STD_ARG_UNDEFINED (0)
#endif /* _STD_ARG_UNDEFINED */

#ifndef _STD_ARG_MISSING
#define _STD_ARG_MISSING (1)
#endif /* _STD_ARG_MISSING */

#ifndef _STD_ARG_FINISHED
#define _STD_ARG_FINISHED (2)
#endif /* _STD_ARG_FINISHED */

#ifndef _STD_VALID_ARG
#define _STD_VALID_ARG(x) (x > _STD_ARG_FINISHED)
#endif /* _STD_VALID_ARG */

typedef struct argument_data {
  const char identifier;
  const char * const restrict access_chars;
  const char * const restrict access_name;
  const char * const restrict value_name;
  const char * const restrict description;
} argument_data;

typedef struct argument_context {
  s32 argc;
  char ** argv;
  const argument_data * const a_data;
  const s32 a_size;
} argument_context;

_STD_FN argument_context arg_init(s32 * const restrict argc,
                                  char *** argv,
                                  const argument_data * const a_data,
                                  const s32 a_size);

_STD_FN char arg_fetch(argument_context * restrict a_ctx);

_STD_FN char * arg_peek(argument_context * restrict a_ctx);

_STD_FN char * arg_peek_forward(argument_context * restrict a_ctx);

_STD_FN char * arg_arg(argument_context * restrict a_ctx);

_STD_FN u0 arg_usage(const argument_context * const restrict a_ctx, const u8 padding);

#ifdef _STD_ARGS_IMPL

_STD_FN char __accepted_args(argument_context * restrict a_ctx, s32 i) {
  if (a_ctx->a_data[i].value_name && (!arg_peek_forward(a_ctx) || *arg_peek_forward(a_ctx) == '-')) {
    return _STD_ARG_MISSING;
  }
  return a_ctx->a_data[i].identifier;
}

_STD_FN argument_context arg_init(s32 * const restrict argc,
                                  char *** argv,
                                  const argument_data * const a_data,
                                  const s32 a_size) {

  argument_context a_ctx = {
    .argc   = *argc,
    .argv   = *argv,
    .a_data = a_data,
    .a_size = a_size
  };
  return a_ctx;
}

_STD_FN char arg_fetch(argument_context * restrict a_ctx) {
  if (a_ctx->argc-- < 1 || !*(++a_ctx->argv)) {
    return _STD_ARG_FINISHED;
  }
  if (**a_ctx->argv != '-') {
    return _STD_ARG_UNDEFINED;
  }
  for (s32 i = 0; i < a_ctx->a_size; ++i) {
    const char * argument = *a_ctx->argv;
    switch (*++argument) {
      default: {
        if (!a_ctx->a_data[i].access_chars || !*argument) {
          break;
        }

        const char * name = a_ctx->a_data[i].access_chars;
        while (*name) {
          if (*argument == *name) {
            ++argument;
            break;
          }
          ++name;
        }
        if (!*argument) {
          return __accepted_args(a_ctx, i);
        }
        break;
      }
      case '-': {
        if (!a_ctx->a_data[i].access_name) {
          break;
        }

        ++argument;
        const char * name = a_ctx->a_data[i].access_name;
        while (*name && *argument) {
          if (*name != *argument) {
            break;
          }
          ++name;
          ++argument;
        }
        if (!*name && !*argument) {
          return __accepted_args(a_ctx, i);
        }
        break;
      }
    }
  }
  return _STD_ARG_UNDEFINED;
}

_STD_FN char * arg_peek(argument_context * restrict a_ctx) {
  if (a_ctx->argc < 1) {
    return NULL;
  }
  return *a_ctx->argv;
}

_STD_FN char * arg_peek_forward(argument_context * restrict a_ctx) {
  if (a_ctx->argc < 2) {
    return NULL;
  }
  return *(a_ctx->argv + 1);
}

_STD_FN char * arg_arg(argument_context * restrict a_ctx) {
  if (a_ctx->argc < 2) {
    return NULL;
  }
  --a_ctx->argc;
  return *++a_ctx->argv;
}

_STD_FN u0 arg_usage(const argument_context * const restrict a_ctx, const u8 padding) {
  for (s32 i = 0; i < a_ctx->a_size; ++i) {
    printf("  ");
    char * delim = "";
    u8 to_pad = padding;
    if (a_ctx->a_data[i].access_chars) {
      for (const char * restrict chars = a_ctx->a_data[i].access_chars; *chars; ++chars) {
        printf("%s-%c", delim, *chars);
        to_pad -= 4 * sizeof(char);
        delim = ", ";
      }
      to_pad += 2; /* first `delim` is not used */
    }
    if (a_ctx->a_data[i].access_name) {
      printf("%s--%s", delim, a_ctx->a_data[i].access_name);
      to_pad -= ((*delim) ? 2 : 0);
      for (const char * tmp = a_ctx->a_data[i].access_name; *tmp; ++tmp) {
        --to_pad;
      }
    }

    if (a_ctx->a_data[i].description) {
      if (a_ctx->a_data[i].value_name) {
        printf(" %s", a_ctx->a_data[i].value_name);
        --to_pad;
        for (const char * tmp = a_ctx->a_data[i].value_name; *tmp; ++tmp) {
          --to_pad;
        }
      }
      printf("%*s%s", to_pad, " ", a_ctx->a_data[i].description);
    }
    putchar('\n');
  }
}

#endif /* _STD_ARGS_IMPL */

#ifndef _STD_TYPES_H
#undef uptr
#undef u64
#undef u8
#undef u0
#endif

#undef _STD_FN

#ifdef __cplusplus
};
#endif

#endif /* _STD_ARGS_H */
