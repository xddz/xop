/* * * * * * * * * * * * * * * * *
 *       _       _       _       *
 *  ___ | |_  __| |     | |__    *
 * / __|| __|/ _` |     | '_ \   *
 * \__ \| |_| (_| |  _  | | | |  *
 * |___/ \__|\__,_| (_) |_| |_|  *
 *                               *
 * * * * * * * * * * * * * * * * *
 *       *  _std_log.h  *        *
 * * * * * * * * * * * * * * * * *
 *    https://xddz.pages.dev     *
 * * * * * * * * * * * * * * * * *
 *    _STD_FATAL("");            *
 *    _STD_ERROR("");            *
 *    _STD_INFO("");             *
 *    _STD_DEBUG("");            *
 *    _STD_TRACE("");            *
 *    _STD_ASSERT(expr);         *
 *    _STD_ASSERT_MSG(expr, ""); *
 *    _STD_ASSERT_DEBUG(expr);   *
 *                               *
 *                               *
 *                               *
 * * * * * * * * * * * * * * * * */

#ifndef _STD_LOG_H
#define _STD_LOG_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _STD_FN
#define _STD_FN inline static
#endif

#ifndef _STD_TYPES_H
#include <inttypes.h>

#ifndef u64
#define u64  uint64_t
#endif

#ifndef u32
#define u32  uint32_t
#endif

#ifndef u16
#define u16  uint16_t
#endif

#ifndef u8
#define u8   uint8_t
#endif

#ifndef s64
#define s64   int64_t
#endif

#ifndef s32
#define s32   int32_t
#endif

#ifndef s16
#define s16   int16_t
#endif

#ifndef s8
#define s8    int8_t
#endif

#ifndef b64
#define b64   u64
#endif

#ifndef b32
#define b32   u32
#endif

#ifndef b16
#define b16   u16
#endif

#ifndef b8
#define b8    u8
#endif

#ifndef f64
#define f64 double
#endif

#ifndef f32
#define f32 float
#endif

#ifndef u0
#define u0   void
#endif

#ifndef true
#define true (1)
#endif

#ifndef false
#define false (0)
#endif

#endif /* _STD_TYPES_H */

#include <stdio.h>
#include <stdarg.h>

#if defined(_STD_PLATFORM_WINDOWS) || defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
#include <windef.h>
#include <winbase.h>
#include <wincon.h>
#endif

#define  _STD_LOG_WARN_ENABLED
#define  _STD_LOG_INFO_ENABLED

#ifdef  _STD_DEBUG
#define _STD_LOG_DEBUG_ENABLED
#define _STD_LOG_TRACE_ENABLED
#endif

typedef enum {
  LOG_LEVEL_FATAL = 0 ,
  LOG_LEVEL_ERROR     ,
  LOG_LEVEL_WARN      ,
  LOG_LEVEL_INFO      ,
  LOG_LEVEL_DEBUG     ,
  LOG_LEVEL_TRACE     ,
} log_level;

#define _STD_LOG_FATAL_REPR "[/]"
#define _STD_LOG_ERROR_REPR "[-]"
#define _STD_LOG_WARN_REPR  "[!]"
#define _STD_LOG_INFO_REPR  "[#]"
#define _STD_LOG_DEBUG_REPR "[~]"
#define _STD_LOG_TRACE_REPR "[>]"

#if defined(_STD_PLATFORM_UNIX) || defined(__ANDROID__) || defined(__linux__) || defined(__unix) || defined(_POSIX_VERSION)
typedef enum platform_color {
  _STD_COLOR_GREEN       = 32,
  _STD_COLOR_YELLOW      = 33,
  _STD_COLOR_RED         = 31,
  _STD_COLOR_BLUE        = 34,
  _STD_COLOR_PURPLE      = 35,
  _STD_COLOR_WHITE       = 37,

  _STD_BACKGROUND_GREEN  = 42,
  _STD_BACKGROUND_YELLOW = 43,
  _STD_BACKGROUND_RED    = 41,
  _STD_BACKGROUND_BLUE   = 44,
  _STD_BACKGROUND_PURPLE = 45,
  _STD_BACKGROUND_WHITE  = 47,
} platform_color;

#elif defined(_STD_PLATFORM_WINDOWS) || defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)

typedef enum platform_color {
  _STD_COLOR_GREEN       = FOREGROUND_GREEN                                      ,
  _STD_COLOR_YELLOW      = (FOREGROUND_RED | FOREGROUND_GREEN)                   ,
  _STD_COLOR_RED         = FOREGROUND_RED                                        ,
  _STD_COLOR_BLUE        = FOREGROUND_BLUE                                       ,
  _STD_COLOR_PURPLE      = (FOREGROUND_RED | FOREGROUND_BLUE)                    ,
  _STD_COLOR_WHITE       = (FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE) ,

  _STD_BACKGROUND_GREEN  = BACKGROUND_GREEN                                      ,
  _STD_BACKGROUND_YELLOW = (BACKGROUND_RED | BACKGROUND_GREEN)                   ,
  _STD_BACKGROUND_RED    = BACKGROUND_RED                                        ,
  _STD_BACKGROUND_BLUE   = BACKGROUND_BLUE                                       ,
  _STD_BACKGROUND_PURPLE = (BACKGROUND_RED | BACKGROUND_BLUE)                    ,
  _STD_BACKGROUND_WHITE  = (BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE) ,
} platform_color;

#else

typedef enum platform_color {
  _STD_COLOR_GREEN       = 0,
  _STD_COLOR_YELLOW      = 0,
  _STD_COLOR_RED         = 0,
  _STD_COLOR_BLUE        = 0,
  _STD_COLOR_PURPLE      = 0,
  _STD_COLOR_WHITE       = 0,

  _STD_BACKGROUND_GREEN  = 0,
  _STD_BACKGROUND_YELLOW = 0,
  _STD_BACKGROUND_RED    = 0,
  _STD_BACKGROUND_BLUE   = 0,
  _STD_BACKGROUND_PURPLE = 0,
  _STD_BACKGROUND_WHITE  = 0,
} platform_color;
#endif

/* logs a fatal-level message  */ 
#ifndef _STD_FATAL
#define _STD_FATAL(message, ...) log_output(LOG_LEVEL_FATAL, message, ##__VA_ARGS__)
#endif
/* * * * * * * * * * * * * * * */

/* logs an error-level message */ 
#ifndef _STD_ERROR
#define _STD_ERROR(message, ...) log_output(LOG_LEVEL_ERROR, message, ##__VA_ARGS__)
#endif
/* * * * * * * * * * * * * * * */

/* logs a warning-level message  */ 
#ifndef _STD_WARN
#ifdef  _STD_LOG_WARN_ENABLED
#define _STD_WARN(message, ...) log_output(LOG_LEVEL_WARN, message, ##__VA_ARGS__)
#else
#define _STD_WARN(message, ...)
#endif
#endif
/* * * * * * * * * * * * * * * * */

/* logs an info-level message  */ 
#ifndef _STD_INFO
#ifdef  _STD_LOG_INFO_ENABLED
#define _STD_INFO(message, ...) log_output(LOG_LEVEL_INFO, message, ##__VA_ARGS__)
#else
#define _STD_INFO(message, ...)
#endif
#endif
/* * * * * * * * * * * * * * * */

/* logs a debug-level message  */ 
#ifndef _STD_DEBUG
#ifdef  _STD_LOG_DEBUG_ENABLED
#define _STD_DEBUG(message, ...) log_output(LOG_LEVEL_DEBUG, message, ##__VA_ARGS__)
#else
#define _STD_DEBUG(message, ...)
#endif
#endif
/* * * * * * * * * * * * * * * */

/* logs a trace-level message  */ 
#ifndef _STD_TRACE
#ifdef  _STD_LOG_TRACE_ENABLED
#define _STD_TRACE(message, ...) log_output(LOG_LEVEL_TRACE, message, ##__VA_ARGS__)
#else
#define _STD_TRACE(message, ...)
#endif
#endif
/* * * * * * * * * * * * * * * */

/* 
   "[/]: fatal-level   log output" 
   "[-]: error-level   log output"
   "[!]: warning-level log output"
   "[#]: info-level    log output"
   "[~]: debug-level   log output"
   "[>]: trace-level   log output"
*/

#if defined(_STD_PLATFORM_UNIX) || defined(__ANDROID__) || defined(__linux__) || defined(__unix) || defined(_POSIX_VERSION)
_STD_FN u0 _STD_COLOR(u8 color, b8 bold){
  /* fatal, error, warn, info, debug, trace */
  printf("\033[%d;%dm", bold, color);
}

_STD_FN u0 _STD_BLANK(u0){
  printf("\033[0;0m");
}

#elif defined(_STD_PLATFORM_WINDOWS) || defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)

static HANDLE _h_console = 0;
static WORD _saved_attributes;

_STD_FN u0 _STD_COLOR(const u8 color, b8 bold) {
  if (!_h_console) {
    _h_console = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
    GetConsoleScreenBufferInfo(_h_console, &consoleInfo);
    _saved_attributes = consoleInfo.wAttributes;
  }
  SetConsoleTextAttribute(_h_console,
            (bold) ? (color | FOREGROUND_INTENSITY) : (color));
}

_STD_FN u0 _STD_BLANK(u0) {
  SetConsoleTextAttribute(_h_console, _saved_attributes);
}

#else

_STD_FN u0 _STD_COLOR(u8 color){
  /* fatal, error, warn, info, debug, trace */
  return;
}

_STD_FN u0 _STD_BLANK(u0){
  return;
}
#endif

_STD_FN u0 log_output(log_level level, const char * const message, ...){
  const char * level_repr[] = {
    [LOG_LEVEL_FATAL] = _STD_LOG_FATAL_REPR ": ",
    [LOG_LEVEL_ERROR] = _STD_LOG_ERROR_REPR ": ",
    [LOG_LEVEL_WARN]  = _STD_LOG_WARN_REPR  ": ",
    [LOG_LEVEL_INFO]  = _STD_LOG_INFO_REPR  ": ",
    [LOG_LEVEL_DEBUG] = _STD_LOG_DEBUG_REPR ": ",
    [LOG_LEVEL_TRACE] = _STD_LOG_TRACE_REPR ": "
  };

  const u8 map[] = {
    [LOG_LEVEL_FATAL] = _STD_BACKGROUND_RED,
    [LOG_LEVEL_ERROR] = _STD_COLOR_RED,
    [LOG_LEVEL_WARN]  = _STD_COLOR_YELLOW,
    [LOG_LEVEL_INFO]  = _STD_COLOR_GREEN,
    [LOG_LEVEL_DEBUG] = _STD_COLOR_BLUE,
    [LOG_LEVEL_TRACE] = _STD_BACKGROUND_WHITE,
  };

  /* NOTE: this is a workaround 
     for overwritten va_list on windows */
  __builtin_va_list args; 

  _STD_COLOR(map[level], (level < LOG_LEVEL_WARN));
  va_start(args, message);
  fprintf(stdout, "%s", level_repr[level]);
  vfprintf(stdout, message, args);
  va_end(args);
  _STD_BLANK();
  fprintf(stdout, "\r\n");
}
/* * * * * * * * * * * * * * * * */

#ifdef _STD_DEBUG
#if _MSC_VER
#include <intrin.h>
#define debug_break() __debugbreak()
#else
#define debug_break() __builtin_trap()
#endif

_STD_FN u0 report_assertion_failure(const char * const expression,
                                    const char * const message,
                                    const char * const file,
                                    s32 line){
  log_output(LOG_LEVEL_FATAL,
             "Assertion Failure: %s, message: \"%s\", in file: %s, line %d",
             expression,
             message,
             file,
             line);
}

#define _STD_ASSERT(expr){                                           \
  do{                                                                \
    if (!(expr)){                                                    \
      report_assertion_failure(#expr, "", __FILE__, __LINE__);       \
      debug_break();                                                 \
    }                                                                \
  }while(0);                                                         \
}

#define _STD_ASSERT_MSG(expr, message){                              \
  do{                                                                \
    if (!(expr)){                                                    \
      report_assertion_failure(#expr, message, __FILE__, __LINE__);  \
      debug_break();                                                 \
    }                                                                \
  }while(0);                                                         \
}

#ifdef _STD_DEBUG
#define _STD_ASSERT_DEBUG(expr){                                     \
  do{                                                                \
    if (!(expr)){                                                    \
      report_assertion_failure(#expr, "", __FILE__, __LINE__);       \
      debug_break();                                                 \
    }                                                                \
  }while(0);                                                         \
}
#else
#define _STD_ASSERT_DEBUG(expr)
#endif

#else
#define _STD_ASSERT(expr)
#define _STD_ASSERT_MSG(expr, message)
#define _STD_ASSERT_DEBUG(expr)
#endif

#ifndef _STD_TYPES_H
#undef u64
#undef u32
#undef u16
#undef u8
#undef s64
#undef s32
#undef s16
#undef s8
#undef b64
#undef b32
#undef b16
#undef b8
#undef f64
#undef f32
#undef u0
#undef true
#undef false
#undef _STD_FN
#endif /* _STD_TYPES_H */

#ifdef __cplusplus
};
#endif

#endif /* _STD_LOG_H */
