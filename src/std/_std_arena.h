/* * * * * * * * * * * * * * * * *
 *       _       _       _       *
 *  ___ | |_  __| |     | |__    *
 * / __|| __|/ _` |     | '_ \   *
 * \__ \| |_| (_| |  _  | | | |  *
 * |___/ \__|\__,_| (_) |_| |_|  *
 *                               *
 * * * * * * * * * * * * * * * * *
 *      *  _std_arena.h  *       *
 * * * * * * * * * * * * * * * * *
 *    https://xddz.pages.dev     *
 * * * * * * * * * * * * * * * * *
 * #define _STD_ARENA_IMPL       *
 * #include "_std_arena.h"       *
 *                               *
 * u8 data[99];                  *
 * arena_t arena = {0};          *
 * arena_init(&arena, data, 99); *
 *                               *
 * char * str = \                *
 *     arena_alloc(&arena, 10);  *
 *                               *
 * arena_free(&arena);           *
 * * * * * * * * * * * * * * * * */

#ifndef _STD_ARENA_H
#define _STD_ARENA_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _STD_TYPES_H
#include <inttypes.h>

#ifndef uptr
#define uptr uintptr_t
#endif

#ifndef u64
#define u64 uint64_t
#endif

#ifndef u32
#define u32 uint32_t
#endif

#ifndef u16
#define u16 uint16_t
#endif

#ifndef u8
#define u8 uint8_t
#endif

#ifndef s64
#define s64 int64_t
#endif

#ifndef s32
#define s32 int32_t
#endif

#ifndef s16
#define s16 int16_t
#endif

#ifndef s8
#define s8 int8_t
#endif

#ifndef b64
#define b64 u64
#endif

#ifndef b32
#define b32 u32
#endif

#ifndef b16
#define b16 u16
#endif

#ifndef b8
#define b8 u8
#endif

#ifndef f64
#define f64 double
#endif

#ifndef f32
#define f32 float
#endif

#ifndef u0
#define u0 void
#endif

#ifndef true
#define true (1)
#endif

#ifndef false
#define false (0)
#endif

#endif /* _STD_TYPES_H */

#ifndef _STD_LOG_H
#include <stdio.h>
#endif /* _STD_LOG_H */

#ifndef _STD_STRING_H
#include <string.h>
#endif /* _STD_STRING_H */

#ifndef _STD_FN
#define _STD_FN inline static
#endif

#ifndef _STD_ARENA_IS_POWER_OF_TWO
#define _STD_ARENA_IS_POWER_OF_TWO(x) ((x & (x-1)) == 0)
#endif

#ifndef _STD_ARENA_DEFAULT_ALIGNMENT
#define _STD_ARENA_DEFAULT_ALIGNMENT (2 * sizeof(u0 *))
#endif

/* align `ptr` forward by `align` power of two */
_STD_FN uptr align_forward(uptr ptr, u64 align) {
  /* verify `align` is a power of two  */
  if (!_STD_ARENA_IS_POWER_OF_TWO(align)) {
    return 0;
  }
  /* * * * * * * * * * * * * * * * * * */

  uptr p = ptr;
  uptr a = (uptr)align;

  /* same as (p % a) but faster
   * since `a` is a power of two */
	uptr modulo = p & (a-1);

  /* if `p` is not aligned
   * push the address to the next aligned value  */
  if (modulo != 0) {
    p += (a - modulo);
  }
  /* * * * * * * * * * * * * * * * * * * * * * * */
  return p;
}
/* * * * * * * * * * * * * * * * * * * * * * * */

/* arena data-structure  */
typedef struct arena_t {
  u8  * data;
  u64   length;
  u64   previous_offset;
  u64   current_offset;
} arena_t;
/* * * * * * * * * * * * */

/* main functions  */
_STD_FN u0 arena_init(arena_t * arena, u0 * memory, u64 length);

_STD_FN u0 * arena_alloc_align(arena_t * arena,
                               const u64 size,
                               const u64 align);

_STD_FN u0 * arena_alloc(arena_t * arena,
                         const u64 size);

_STD_FN u0 * arena_resize_align(arena_t * arena,
                                u0 * old_memory,
                                const u64 old_size,
                                const u64 new_size,
                                const u64 align);

_STD_FN u0 * arena_resize(arena_t * arena,
                          u0 * old_memory,
                          const u64 old_size,
                          const u64 new_size);

_STD_FN u0 arena_free(arena_t * arena);
/* * * * * * * * * */

#ifdef _STD_ARENA_IMPL
/* initialize arena with pre-allocated memory buffer */
_STD_FN u0 arena_init(arena_t * arena, u0 * memory, u64 length) {
  arena->data            = (u8 *)memory;
  arena->length          = length;
  arena->current_offset  = 0;
  arena->previous_offset = 0;
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * */

/* allocate `size` bytes from `arena` with custom alignment  */
_STD_FN u0 * arena_alloc_align(arena_t * arena,
                               const u64 size,
                               const u64 align) {
  /* obtain aligned `current_offset` */
  uptr current_ptr     = (uptr)arena->data + (uptr)arena->current_offset;
  uptr aligned_offset  = align_forward(current_ptr, align);
  aligned_offset      -= (uptr)arena->data; /* relative offset */
  /* * * * * * * * * * * * * * * * * */
  
  /* verify arena has enough space left  */
  if ((aligned_offset + size) <= arena->length) {
    u0 * ptr               = &arena->data[aligned_offset];
    arena->previous_offset = aligned_offset;
    arena->current_offset  = (aligned_offset + size);
  
    /* zero memory by default */
#ifdef _STD_STRING_H
    _STD_MEMSET(ptr, 0, size);
#else
    memset(ptr, 0, size);
#endif
    return ptr;
  }
  /* * * * * * * * * * * * * * * * * * * */
  return NULL;
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* allocate using the default memory alignment */
_STD_FN u0 * arena_alloc(arena_t * arena,
                         const u64 size) {
  return arena_alloc_align(arena, size, _STD_ARENA_DEFAULT_ALIGNMENT);
}
/* * * * * * * * * * * * * * * * * * * * * * * */

/* resize arena memory with custom alignment */
_STD_FN u0 * arena_resize_align(arena_t * arena,
                                u0 * old_memory,
                                const u64 old_size,
                                const u64 new_size,
                                const u64 align) {
  if (!_STD_ARENA_IS_POWER_OF_TWO(align)) {
    return 0;
  }
  
  u8 * old_mem = (u8 *)old_memory;
  if (!old_mem) {
    return arena_alloc_align(arena, new_size, align);
  }

  if (arena->data <= old_mem && old_mem < (arena->data + arena->length)) {
    /* if old_mem corresponds to the previously allocated memory  */
    if ((arena->data + arena->previous_offset) == old_mem) {
      arena->current_offset = (arena->previous_offset + new_size);
      if (new_size > old_size) {
        /* zero memory by default */
#ifdef _STD_STRING_H
        _STD_MEMSET(&arena->data[arena->current_offset], 0, (new_size - old_size));
#else
        memset(&arena->data[arena->current_offset], 0, (new_size - old_size));
#endif
      }
      return old_memory;
    }
    /* * * * * * * * * * * * * * * * * * * * * * * * * ** * * * * */

    u0 * new_memory = arena_alloc_align(arena, new_size, align);
    const u64 copy_size = ((old_size < new_size) ? old_size : new_size);
    // Copy across old memory to the new memory
#ifdef _STD_STRING_H
    _STD_MEMMOVE(new_memory, old_memory, copy_size);
#else
    memmove(new_memory, old_memory, copy_size);
#endif
    return new_memory;
  }

#ifdef _STD_LOG_H
  _STD_FATAL("Arena memory length is out of bounds");
#else
  fprintf(stderr, "Arena memory length is out of bounds\n");
#endif
  return NULL;
}
/* * * * * * * * * * * * * * * * * * * * * * */

/* resize arena with default alignment */
u0 * arena_resize(arena_t * arena,
                  u0 * old_memory,
                  const u64 old_size,
                  const u64 new_size) {
  return arena_resize_align(arena, old_memory, old_size, new_size, _STD_ARENA_DEFAULT_ALIGNMENT);
}
/* * * * * * * * * * * * * * * * * * * */

/* free arena  */
_STD_FN u0 arena_free(arena_t * arena) {
  arena->current_offset  = 0;
  arena->previous_offset = 0;
}
/* * * * * * * */
#endif

#ifndef _STD_TYPES_H
#undef uptr
#undef u64
#undef u32
#undef u16
#undef u8
#undef s64
#undef s32
#undef s16
#undef s8
#undef b64
#undef b32
#undef b16
#undef b8
#undef f64
#undef f32
#undef u0
#undef true
#undef false
#endif /* _STD_TYPES_H */

#undef _STD_FN
#undef _STD_ARENA_IS_POWER_OF_TWO
#undef _STD_ARENA_DEFAULT_ALIGNMENT

#ifdef __cplusplus
};
#endif

#endif /* _STD_ARENA_H */
