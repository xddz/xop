/* * * * * * * * * * * * * * * * *
 *       _       _       _       *
 *  ___ | |_  __| |     | |__    *
 * / __|| __|/ _` |     | '_ \   *
 * \__ \| |_| (_| |  _  | | | |  *
 * |___/ \__|\__,_| (_) |_| |_|  *
 *                               *
 * * * * * * * * * * * * * * * * *
 *      *  _std_types.h  *       *
 * * * * * * * * * * * * * * * * *
 *    https://xddz.pages.dev     *
 * * * * * * * * * * * * * * * * *
 *    u0    **  uptr  **         *
 *    u8l   **  u8f   **  u8     *
 *    u16l  **  u16f  **  u16    *
 *    u32l  **  u32f  **  u32    *
 *    u64l  **  u64f  **  u64    *
 *    s8l   **  s8f   **  s8     *
 *    s16l  **  s16f  **  s16    *
 *    s32l  **  s32f  **  s32    *
 *    s64l  **  s64f  **  s64    *
 *    f32   **  f64   **         *
 *    b8    **  b32   **         *
 *    true  **  false **         *
 * * * * * * * * * * * * * * * * */

#ifndef _STD_TYPES_H
#define _STD_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <inttypes.h>

/* void alias  */
typedef void u0;
/* * * * * * * */

/* unsigned integer of size pointer  */
typedef uintptr_t uptr;
/* * * * * * * * * * * * * * * * * * */

/* unsigned 8-bit integers */
typedef uint_least8_t u8l;
typedef uint_fast8_t  u8f;
typedef uint8_t       u8;
/* * * * * * * * * * * * * */

/* unsigned 16-bit integers  */
typedef uint_least16_t u16l;
typedef uint_fast16_t  u16f;
typedef uint16_t       u16;
/* * * * * * * * * * * * * * */

/* unsigned 32-bit integers  */
typedef uint_least32_t u32l;
typedef uint_fast32_t  u32f;
typedef uint32_t       u32;
/* * * * * * * * * * * * * * */

/* unsigned 64-bit integers  */
typedef uint_least64_t u64l;
typedef uint_fast64_t  u64f;
typedef uint64_t       u64;
/* * * * * * * * * * * * * * */

/* signed 8-bit integers */
typedef int_least8_t s8l;
typedef int_fast8_t  s8f;
typedef int8_t       s8;
/* * * * * * * * * * * * */

/* signed 16-bit integers  */
typedef int_least16_t s16l;
typedef int_fast16_t  s16f;
typedef int16_t       s16;
/* * * * * * * * * * * * * */

/* signed 32-bit integers  */
typedef int_least32_t s32l;
typedef int_fast32_t  s32f;
typedef int32_t       s32;
/* * * * * * * * * * * * * */

/* signed 64-bit integers  */
typedef int_least64_t s64l;
typedef int_fast64_t  s64f;
typedef int64_t       s64;
/* * * * * * * * * * * * * */

/* floating-point numbers  */
typedef float  f32;
typedef double f64;
/* * * * * * * * * * * * * */

/* booleans  */
typedef u8  b8;
typedef u32 b32;
typedef u64 b64;
/* * * * * * */

#ifdef __cplusplus
};
#endif

#endif /* _STD_TYPES_H */
