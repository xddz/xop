/* * * * * * * * * * * * * * * * *
 *       _       _       _       *
 *  ___ | |_  __| |     | |__    *
 * / __|| __|/ _` |     | '_ \   *
 * \__ \| |_| (_| |  _  | | | |  *
 * |___/ \__|\__,_| (_) |_| |_|  *
 *                               *
 * * * * * * * * * * * * * * * * *
 *      *   _std_str.h   *       *
 * * * * * * * * * * * * * * * * *
 *    https://xddz.pages.dev     *
 * * * * * * * * * * * * * * * * *
 * #define _STD_STR_IMPL         *
 * #define _STD_ALLOCATE xxx     *
 * #define _STD_REALLOCATE xxx   *
 * #define _STD_FREE xxx         *
 * #define _STD_STRNCPY xxx      *
 * #define _STD_STRNCAT xxx      *
 *                               *
 * string s;                     *
 * s = string_make("Hello, ");   *
 * printf("{`%s`, %d}\n",\       *
 *        s.data, s.length);     *
 *                               *
 * string_add(&s, "World", 0);   *
 * printf("{`%s`, %d}\n",\       *
 *        s.data, s.length);     *
 * string_free(s.data,\          *
 *             s.length);        *
 * * * * * * * * * * * * * * * * */

#ifndef _STD_STR_H
#define _STD_STR_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _STD_TYPES_H
#include <inttypes.h>

#ifndef uptr
#define uptr uintptr_t
#endif

#ifndef u64
#define u64 uint64_t
#endif

#ifndef u32
#define u32 uint32_t
#endif

#ifndef u16
#define u16 uint16_t
#endif

#ifndef u8
#define u8 uint8_t
#endif

#ifndef s64
#define s64 int64_t
#endif

#ifndef s32
#define s32 int32_t
#endif

#ifndef s16
#define s16 int16_t
#endif

#ifndef s8
#define s8 int8_t
#endif

#ifndef b64
#define b64 u64
#endif

#ifndef b32
#define b32 u32
#endif

#ifndef b16
#define b16 u16
#endif

#ifndef b8
#define b8 u8
#endif

#ifndef f64
#define f64 double
#endif

#ifndef f32
#define f32 float
#endif

#ifndef u0
#define u0 void
#endif

#ifndef true
#define true (1)
#endif

#ifndef false
#define false (0)
#endif

#endif /* _STD_TYPES_H */

#ifndef _STD_FN
#define _STD_FN inline static
#endif

#ifndef _STD_WARN
#define _I_STD_WARN
#include <stdio.h>
#define _STD_WARN(x) fprintf(stderr, x)
#endif

#ifndef _STD_ALLOCATE
#define _STD_INTERNAL_ALLOCATIONS
#include <stdlib.h>
#define _STD_ALLOCATE(buffer, length) buffer = malloc(length)
#endif /* _STD_ALLOCATE */

#ifndef _STD_REALLOCATE
#ifndef _STD_INTERNAL_ALLOCATIONS
#define _STD_INTERNAL_ALLOCATIONS
#include <stdlib.h>
#endif
#define _STD_REALLOCATE(buffer, length) buffer = realloc(buffer, length)
#endif /* _STD_REALLOCATE */

#ifndef _STD_FREE
#ifndef _STD_INTERNAL_ALLOCATIONS
#define _STD_INTERNAL_ALLOCATIONS
#include <stdlib.h>
#endif
#define _STD_FREE(buffer, length) free(buffer)
#endif /* _STD_FREE */

#ifndef _STD_STRNCPY
#define _STD_INTERNAL_STRFN
#include <string.h>
#define _STD_STRNCPY(destination, source, length) strncpy(destination, source, length)
#endif /* _STD_STRNCPY */

#ifndef _STD_STRNCAT
#ifndef _STD_INTERNAL_STRFN
#define _STD_INTERNAL_STRFN
#include <string.h>
#endif
#define _STD_STRNCAT(destination, source, length) strncat(destination, source, length)
#endif /* _STD_STRNCAT */

typedef struct string {
  u64 length;
  char * data;
} string;

/* obtain length from c-string */
_STD_FN u64 cstring_length(const char * const cstring);
/* * * * * * * * * * * * * * * */

/* concatenate two c-strings */
_STD_FN string cstring_add(char * destination,
                           u64 destination_length,
                           char * source,
                           u64 source_length);
/* * * * * * * * * * * * * * */

/* create a string */
_STD_FN string string_make(const char * const cstring);
/* * * * * * * * * */

/* concatenate a c-string into a string  */
_STD_FN u0 string_add(string * destination,
                      char * source,
                      u64 source_length);
/* * * * * * * * * * * * * * * * * * * * */

/* free a string */
_STD_FN u0 string_free(char * data,
                       const u64 length);
/* * * * * * * * */

#ifdef _STD_STR_IMPL
_STD_FN u64 cstring_length(const char * const cstring) {
  if (!cstring) {
    return 0;
  }

  const char * tmp = cstring;
  for (; *tmp; ++tmp) {}
  return (tmp - cstring);
}

_STD_FN string cstring_add(char * destination,
                           u64 destination_length,
                           char * source,
                           u64 source_length) {
  if (!destination || !source) {
    return (string){};
  }

  if (!destination_length) {
    destination_length = cstring_length(destination);
  }

  if (!source_length) {
    source_length = cstring_length(source);
  }

  string created_string;
  created_string.length = (destination_length + source_length);
  _STD_ALLOCATE(created_string.data, created_string.length);
  _STD_STRNCPY(created_string.data, destination, created_string.length);
  _STD_STRNCAT(created_string.data, source, created_string.length);
  return created_string;
}

_STD_FN char * cstring_read_file(const char * const file_path,
                                 u64 * restrict length) {
  FILE* f = fopen(file_path, "rb");
  if (f) {
    fseek(f, 0, SEEK_END);
    *length = (ftell(f) * sizeof(char));
    fseek(f, 0, SEEK_SET);

    if (*length) {
      /* 1 GiB; best not to load a whole large file in one string */
      if (*length > (1073741824 * sizeof(char))) {
        _STD_WARN("Could not store file: `%s`", file_path);
        fclose(f);
        return (char *)NULL;
      }

      char * contents = _STD_ALLOCATE(contents, *length + sizeof(char));
      const u64 read_length = fread(contents, 1, *length * sizeof(char), f);

      if (*length != read_length) {
        _STD_WARN("Failed reading file: `%s`", file_path);
        _STD_FREE(contents,* length + 1);
        fclose(f);
        return (char *)NULL;
      }
      fclose(f);

      contents[*length] = '\0';
      return contents;
    }
  }
  _STD_WARN("Could not find file: `%s`", file_path);
  return (char *)NULL;
}

_STD_FN string string_make(const char * const cstring) {
  if (!cstring) {
    return (string){};
  }

  string created_string;
  created_string.length = cstring_length(cstring);
  _STD_ALLOCATE(created_string.data, created_string.length);
  _STD_STRNCPY(created_string.data, cstring, created_string.length);
  return created_string;
}

_STD_FN u0 string_add(string * destination,
                      char * source,
                      u64 source_length) {
  if (!destination->data || !source) {
    return;
  }

  if (!destination->length) {
    destination->length = cstring_length(destination->data);
  }

  if (!source_length) {
    source_length = cstring_length(source);
  }

  destination->length += source_length;
  _STD_REALLOCATE(destination->data, destination->length);
  _STD_STRNCAT(destination->data, source, destination->length);
}

_STD_FN u0 string_free(char * data,
                       const u64 length) {
  if (!data) {
    return;
  }
  _STD_FREE(data, length);
}
#endif /* _STD_STR_IMPL */

#ifndef _STD_TYPES_H
#undef u64
#undef u32
#undef u16
#undef u8
#undef s64
#undef s32
#undef s16
#undef s8
#undef b64
#undef b32
#undef b16
#undef b8
#undef f64
#undef f32
#undef u0
#undef true
#undef false
#endif /* _STD_TYPES_H */

#ifdef _I_STD_WARN
#undef _STD_WARN
#endif

#undef _STD_FN

#ifdef __cplusplus
}
#endif

#endif /* _STD_STR_H */
